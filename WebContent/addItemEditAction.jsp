<%-- 
    Document   : patient
    Created on : May 25, 2015, 12:36:15 AM
    Author     : amol
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="static com.dbcon.MyDriver.*"%>
<%@page import="java.sql.*"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Doctor Plus</title>

<script src="js/jquery.min.js"></script>


<style>
.footer {
	height: 40px;
	width: 100%;
	color: white;
	background-color: #191919;
	margin-top: 12%;
	text-align: center;
}

.styleP {
	font-size: 50px;
	color: red;
}

.maincontainer {
	height: 70%px;
	width: 90%;
	margin-left: 10%;
}

.maincontainertable {
	width: 85%;
	background-color: #79b7e7;
}

.medications {
	border: 2px;
	alignment-adjust: auto;
	margin-left: 10%;
	overflow: scroll;
	width: 600px;
	height: 180px;
}

body {
	background-color: #cccccc;
}

.endtable {
	width: 700px;
	height: 250px;
	margin-left: 5%;
}
</style>

<!--        <link rel="stylesheet" href="css/jquery-ui.css">
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/jquery.min.js"></script>-->

<script>
    $( "#other" ).click(function() {
  $( "#target" ).scroll();
});
</script>
</head>
<body>

	<%@include file="menu.jsp"%>

<%
if(session.getAttribute("User_Name")== null)
{
	response.sendRedirect("index.jsp");
}
%>
<% 

String idStr= request.getParameter("idhid");
String idCategoryStr= request.getParameter("categoryhid");
String nameCategoryStr= request.getParameter("categoryNamehid");
String itemnameStr= request.getParameter("itemhid");
String levelStr= request.getParameter("levelhid");
String descriptionStr= request.getParameter("descriptionhid");


%>


	<div class="maincontainer">
		<h3>
			<b>Add Item</b>
		</h3>
		<br />

		<form action="addItemUpadateAction.jsp">

			<table class="endtable">

				<tr>

					<th style="text-align: right;">Category&nbsp;&nbsp;&nbsp;</th>
					<td>
						<%

						BigDecimal id=null;
						if(session!=null){  
							 id = (BigDecimal) session.getAttribute("Clinic_Id");
							 System.out.println("id = " + id);
						}

Exception ex=null;
boolean flag=true;
try
{

   String sql= "select * from category where clinic_id="+id ;    
	Class.forName(DB_DRIVER);
	Connection con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS);  
  
    		   Statement st=con.createStatement();
    		    ResultSet rs=st.executeQuery(sql);
	%> <select name="category" style="width: 160px;" >

							<option>-- Select --</option>

							<% while(rs.next()) {%>
							
							<%if(nameCategoryStr.equals(rs.getString("itemcategory"))){ %>           
            <option  selected="selected" value="<%=rs.getBigDecimal("id")%>"><%=nameCategoryStr%></option>  
        <%    rs.next();       } %> 
							

							<option value="<%=rs.getBigDecimal("id")%>"><%=rs.getString("itemcategory")%></option>
							<% }%>

					</select> <%
 
    st.close();
    con.close();
}
catch(Exception e)    
{
   out.println(e);
   ex=e;
   flag=false;
}  

%>

					</td>
				</tr>



				<tr>

					<th style="text-align: right;">Item type&nbsp;&nbsp;&nbsp;</th>
					<td><input type="text" style="width: 160px" name="itemname"  value="<%=itemnameStr %>" ></td>
				</tr>
				
				<tr>

					<th style="text-align: right;">Reorder Level&nbsp;&nbsp;&nbsp;</th>
					<td><input type="text" style="width: 160px" name="level"  value="<%=levelStr %>" ></td>
				</tr>

                <tr> 
                    <th style= "text-align: right;"> Description&nbsp;&nbsp;&nbsp;</th>
                    <td><textarea name="description" id="description" rows="3" cols="23"><%=descriptionStr%></textarea></td>
               </tr>

				<tr>
					<th style="text-align: right;"></th>

					<td><input type='image' height='50' width="130"
						src='images/submit.png'><br /></td>
				</tr>

				

			</table>
		</form>
		
		            <center>
	               <%@include file="showAddItem.jsp"%>
		           </center>
		
	</div>
	<div class="footer">

		<div style="padding: 10px 5px 15px 20px;">© 2014 Scripting Logic
			- All Rights Reserved.</div>
</body>
</html>
