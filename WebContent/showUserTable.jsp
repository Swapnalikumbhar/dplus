<%-- 
    Document   : patient
    Created on : May 25, 2015, 12:36:15 AM
    Author     : amol
--%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="static com.dbcon.MyDriver.*"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"  type="text/css" href="css/tablescroll.css">
</head>
<body>

	

	<div class="maincontainer">
	<br />

		
		<table class="fixed_headers" style="width:70%">
		<thead>
			<tr>
				
				<td>Name</td>
				<td>Address</td>
				<td>Contact</td>
				<td>Email</td>
				<td>Designation</td>
				
				<td></td>
			</tr>
			</thead>



		<tbody>

			<%
         try
           {
        	 BigDecimal id=null;
        	 if(session!=null)
        	 {  
        	 	 id = (BigDecimal) session.getAttribute("Clinic_Id");
        	 	 //System.out.println("id = " + id);
        	 }
        	 Class.forName(DB_DRIVER);
              Connection con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS);
              Statement st=con.createStatement();
          
            String sql ="select * from clinic_user where clinic_id="+id;
       
           
            ResultSet rs =st.executeQuery(sql);
            
            
            while(rs.next())
            {
            %>

			<tr>
				
				<td><%=rs.getString("name")%></td>

				<td><%=rs.getString("address")%></td>
				<td><%=rs.getBigDecimal("contact_no")%></td>
				<td><%=rs.getString("email_id")%></td>


				<td><%=rs.getString("designation")%></td>
				
				<td>
					<form method="post" action="imageServletUser">
					
				<input type="hidden" value="<%=rs.getBigDecimal("id")%>" name="id">
                <input type="hidden" value="<%=rs.getString("name")%>" name="name">
                <input type="hidden" value="<%=rs.getString("gender")%>" name="gender">
                <input type="hidden" value="<%=rs.getDate("date_of_birth")%>" name="dob">
                
                <input type="hidden" value="<%=rs.getString("address")%>" name="address">
                <input type="hidden" value="<%=rs.getBigDecimal("contact_no")%>" name="contact">
                <input type="hidden" value="<%=rs.getString("email_id")%>" name="email">
                
                
                <input type="hidden" value="<%=rs.getString("designation")%>" name="designation">
                <input type="hidden" value="<%=rs.getBlob("profile_photo")%>" name="pphoto">
                <input type="hidden" value="<%=rs.getString("password")%>" name="pass">
          		
                <input type="image" src="images/edit.png">
                
					</form>
					

				</td>


			</tr>


			<%}
            
            rs.close();
            st.close();
            con.close();
           }
           catch(Exception e)
           {
               out.println(e);
            }
          %>
		
		</tbody>

		</table>


	</div>
	
</body>
</html>
