<%@page import="java.math.BigDecimal"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="static com.dbcon.MyDriver.*"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.sql.*"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<% 
   
    BigDecimal id=null;
	if(session!=null)
	{  
	 id = (BigDecimal) session.getAttribute("Clinic_Id");
	}

    String name = request.getParameter("ptname");
    String contact = request.getParameter("contact");
    String date = request.getParameter("tdate");
    String address = request.getParameter("address");
    String age = request.getParameter("age");
    String gender = request.getParameter("gender");
    String weight = request.getParameter("weight");
    String pluse = request.getParameter("pluse");
    String bpmax = request.getParameter("bpmax");
    String bpmin = request.getParameter("bpmin");
    String description = request.getParameter("descripation");
    String fees = request.getParameter("fees");
    String nextdate = request.getParameter("nextdate");
    
    System.out.println("name = " + name);
    String srow = request.getParameter("rcount");
    if(srow==null)
    {
    	srow=""+0;
    }
//	System.out.println("row count = " + srow);
	String medicine = request.getParameter("medicine");
	String mpill = request.getParameter("mpill");
	String apill = request.getParameter("apill");
	String npill = request.getParameter("npill");
	String total = request.getParameter("total");
	int row =0;
	//System.out.println("medicine = "+ medicine+" mpill= "+mpill+" apill = "+apill+" npill= "+npill+ " total= "+ total);;
	
	row= Integer.parseInt(srow);
	
	//int =1;
	String[] medicineArr = new String[row];
	String[] mpillArr = new String[row];
	String[] apillArr = new String[row];
	String[] npillArr = new String[row];
	String[] totalArr = new String[row];
	
	for(int i=0 ,j=1;i<row;i++,j++)
	{
		medicineArr[i] = request.getParameter("medicine"+j);
		//System.out.println(medicineArr[i]);
		mpillArr[i] = request.getParameter("mpill"+j);
		//System.out.println(mpillArr[i]);
		apillArr[i] = request.getParameter("apill"+j);
		//System.out.println(apillArr[i]);
		npillArr[i] = request.getParameter("npill"+j);
		//System.out.println(npillArr[i]);
		totalArr[i] = request.getParameter("total"+j);
		//System.out.println(totalArr[i]);
	}
	
	
	
    
    BigDecimal pid =null;
    BigDecimal tid =null;
    Exception ex=null;
    boolean flag=true;
   try
   {

      DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
       java.util.Date pdate = df.parse(date);
      // System.out.println("date inserted= " + pdate);
     java.sql.Date d1= new java.sql.Date(pdate.getTime());
     //System.out.println("date inserted= " + d1);
   
    
    
     
     DateFormat df1 = new SimpleDateFormat("MM/dd/yyyy");
     java.util.Date pdate1 = df.parse(nextdate);
    // System.out.println("date inserted= " + pdate1);
   java.sql.Date d2= new java.sql.Date(pdate1.getTime());
  // System.out.println("date inserted= " + d2);
     
	   	Class.forName(DB_DRIVER);
	 	Connection con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS); 
	 	Statement st=con.createStatement();
	 	String getPatientId = "select patient_id from clinic_patient where patient_name='"+name+"' and clinic_id="+id;
	 	ResultSet rs = st.executeQuery(getPatientId);
	 	while(rs.next())
	 	{
	 		pid = rs.getBigDecimal("patient_id");
	 	}
	 	
	 	
	 	
	 	String Sql="insert into treatment(patient_name,patient_address,patient_contact,patient_age,patient_gender,patient_weight,patient_bpmax,patient_bpmin,patient_pluse,treatment_date,descripation,fees,next_apt_visit,clinic_id) values ('"+name+"',"+pid+","+pid+","+age+","+pid+","+weight+","+bpmax+","+bpmin+",'"+pluse+"',?,'"+description+"',"+fees+",?,"+id+")";    
	  	PreparedStatement statement = con.prepareStatement(Sql);
	  	
	 	statement.setDate(1, d1);
       	statement.setDate(2, d2);
       	int row1 = statement.executeUpdate();
       	String getTreatmentId=" select treatment_id from treatment where patient_name='"+name+"' and treatment_date='"+d1+"' and clinic_id="+id;
		  System.out.println(Sql);
		  ResultSet rs1 = st.executeQuery(getTreatmentId);
		 	while(rs1.next())
		 	{
		 		tid = rs1.getBigDecimal("treatment_id");
		 	} 
			System.out.println(tid);
			session.setAttribute("tid", tid);

			
		 	String sql1="insert into medications(treatment_id,medicine,morning_pill,afternoon_pill,night_pill,total,clinic_id) values("+tid+",'"+medicine+"',"+mpill+","+apill+","+npill+","+total+","+id+")";
		 	 System.out.println(sql1);
		 	st.executeUpdate(sql1);
		 	
       		if(row >= 1)
       		{
       			for(int i=0 ,j=1;i<row;i++,j++)
       			{
       				String sqli="insert into medications(treatment_id,medicine,morning_pill,afternoon_pill,night_pill,total,clinic_id) values("+tid+",'"+medicineArr[i]+"',"+mpillArr[i]+","+apillArr[i]+","+npillArr[i]+","+totalArr[i]+","+id+")";
       			 System.out.println(sqli);
       				st.executeUpdate(sqli);
       			}
       			
       		}
		 	if (row1 > 0) {
			String message = "data inserted into database";
			//System.out.println(message);
			response.sendRedirect("showTreatmentReport.jsp");
		}
		
		
		 	
		 	
	    st.close();
	    con.close();
   }
   catch(Exception e)    
   {
       out.println(e);
       ex=e;
       flag=false;
   }    
  %>
	


</body>
</html>