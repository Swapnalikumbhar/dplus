<%-- 
    Document   : patient
    Created on : May 25, 2015, 12:36:15 AM
    Author     : amol
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Doctor Plus</title>

<script src="js/jquery.min.js"></script>


<style>
.footer {
	height: 40px;
	width: 100%;
	color: white;
	background-color: #191919;
	margin-top: 12%;
	text-align: center;
}

.styleP {
	font-size: 50px;
	color: red;
}

.maincontainer {
	height: 70%px;
	width: 90%;
	margin-left: 10%;
}

.maincontainertable {
    height: 70%px;
	width: 85%;
	background-color: #79b7e7;
}

.medications {
	border: 2px;
	alignment-adjust: auto;
	margin-left: 10%;
	overflow: scroll;
	width: 600px;
	height: 180px;
}

body {
	background-color: #cccccc;
}

.endtable {
	width: 700px;
	height: 250px;
	margin-left: 5%;
}
</style>

<!--        <link rel="stylesheet" href="css/jquery-ui.css">
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/jquery.min.js"></script>-->

<script>
    $( "#other" ).click(function() {
  $( "#target" ).scroll();
});
</script>
</head>
<body>

	<%@include file="menu.jsp"%>

	<div class="maincontainer">
	<div align="right">
	<label>Welcome <%=session.getAttribute("User_Name") %> </label>
    </div>
	
		<h3>
			<b>Add Expense/Payment</b>
		</h3>
		<br />

		<form action="expenseTypeAddAction.jsp">

			<table class="endtable">

				<tr>

					<th style="text-align: right;">Expense/Payment type&nbsp;&nbsp;&nbsp;</th>
					<td><input type="text" style="width: 150px"
						name="expenseType"></td>
				</tr>


				<tr>
					<th style="text-align: right;"></th>

					<td><input type='image' height='50' width="130"
						src='images/submit.png'><br /></td>
				</tr>


			</table>
		</form>
		
		<center>
		<%@include file="showExpenseType.jsp"%>
		</center>
	</div>
	<div class="footer">

		<div style="padding: 10px 5px 15px 20px;">© 2014 Scripting Logic
			- All Rights Reserved.</div>
</body>
</html>
