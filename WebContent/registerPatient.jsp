<%-- 
    Document   : patient
    Created on : May 25, 2015, 12:36:15 AM
    Author     : amol
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Doctor Plus</title>

<script src="js/jquery.min.js"></script>


        <style>
            .footer {
        height :40px;
        width : 100%;
        color : white;
        background-color: #191919;
        margin-top: 12%;
        text-align: center;

        }
.styleP{
        font-size:50px;
        color : red;
        }
        .maincontainer{
            height:100%;
            width: 90%;
            margin-left: 10%;
           
           
            
        }
        .maincontainertable{
         
            width: 85%;
            background-color: #79b7e7;
            
            
        }
       
        .medications{
            
            border: 2px;
            alignment-adjust: auto;
            margin-left: 10%;
            overflow: scroll; 
            width : 600px;
            height: 180px;
                
        }
        body {
  
    background-color: #cccccc;
}
.endtable{
    width: 750px;
     height: 250px ;
    margin-left: 5%;
}

        </style>
        
<!--        <link rel="stylesheet" href="css/jquery-ui.css">
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/jquery.min.js"></script>-->
<link href="jquery.datepick.package-5.0.0/jquery.datepick.css" rel="stylesheet">
<script src="js/jquery.min.js"></script>
<script src="jquery.datepick.package-5.0.0/jquery.plugin.js"></script>
<script src="jquery.datepick.package-5.0.0/jquery.datepick.js"></script>
<script>
$(function() {
	$('#popupDatepicker').datepick();
        $('#inlineDatepicker').datepick({onSelect: showDate});
});

function showDate(date) {
	alert('The date chosen is ' + date);
}
</script>
  
<script>
    $( "#other" ).click(function() {
  $( "#target" ).scroll();
});
</script>
    </head>
    <body  >
        
     <%@include file="menu.jsp"%>
     <%
if(session.getAttribute("User_Name")== null)
{
	response.sendRedirect("index.jsp");
}
%>
        <div class="maincontainer" >
        <div align="right" style="margin-left: 100px">
	<label>Welcome <%=session.getAttribute("User_Name") %> </label>
 </div>
        
        <h3><b>Add Patient</b></h3>
        <br/>
       
      
        
        <form method="post" action="uploadServlet" enctype="multipart/form-data">

 <table>
       
         <tr>
              <th  style= "text-align: right;">Name of Patient&nbsp;&nbsp;&nbsp; </th>
             <td ><input type="text" style="width: 150px"   name="ptname"></td>
           <th style= "text-align: right;">Contact No.&nbsp;&nbsp;&nbsp;</th>
           <td ><input type="text" style="width: 150px"   name="contact"></td>
        </tr>
         <tr>
             <th style= "text-align: right;">Gender&nbsp;&nbsp;&nbsp; </th>
            <td >	<select name="gender" style="width: 150px">
                    <option value="Select">-- Select --</option>
					<option value="Female">Female</option>
					<option value="Male">Male</option>
					</select>
			</td>
           <th style= "text-align: right;">Marital Status&nbsp;&nbsp;&nbsp;</th>
           <td ><select name="mstatus" style="width: 150px">
           			 <option value="Select">-- Select --</option>
					<option value="Yes">Married</option>
					<option value="No">UnMarried</option>
					</select></td>
        </tr>
         <tr>
             <th style= "text-align: right;">Height&nbsp;&nbsp;&nbsp; </th>
            <td ><input type="text" style="width: 150px"   name="height"></td>
           <th style= "text-align: right;">Occupation&nbsp;&nbsp;&nbsp;</th>
           <td ><input type="text" style="width: 150px"   name="occupation"></td>
        </tr>
        <tr>
             <th style= "text-align: right;">Weight&nbsp;&nbsp;&nbsp; </th>
            <td ><input type="text" style="width: 150px"   name="weight"></td>
           <th style= "text-align: right;">Email&nbsp;&nbsp;&nbsp;</th>
           <td ><input type="text" style="width: 150px"   name="email"></td>
        </tr>
        
        <tr>
             <th style= "text-align: right;">Blood Group &nbsp;&nbsp;&nbsp; </th>
            <td><select name="bgroup" style="width: 150px">
            		 <option value="Select">-- Select --</option>s
					<option value="O+">O+</option>
					<option value="O-">O-</option>
					<option value="A+">A+</option>
					<option value="A-">A-</option>
					<option value="B+">B+</option>
					<option value="B-">B-</option>
					<option value="AB+">AB+</option>
					<option value="AB-">AB-</option>
					</select></td>
            <th style= "text-align: right;">Date Of Birth&nbsp;&nbsp;&nbsp;</th>
           <td ><input type="text" style="width: 150px" id="popupDatepicker"  name="dob"></td>
           
        </tr>
         <tr>
              <th style= "text-align: right;">Patient Picture &nbsp;&nbsp;&nbsp; </th>
              <td ><input type="file" style="width: 250px"   name="photo" value="Browse"></td>
            <th style= "text-align: right;">Doctor Name&nbsp;&nbsp;&nbsp;</th>
           <td>
		         <%
		         BigDecimal id=null;
		         if(session!=null){  
		         	 id = (BigDecimal) session.getAttribute("Clinic_Id");
		         	 //System.out.println("id = " + id);
		         }

		         	Exception ex=null;
					boolean flag=true;
					try
					{
						   String sql= "select name from clinic_user where designation='doctor' and clinic_id="+id ;      
						   Class.forName(DB_DRIVER);
						   Connection con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS);  
		  		   		   Statement st=con.createStatement();
		    		       ResultSet rs=st.executeQuery(sql);
				%> 
					<select name="dname" style="width: 160px;">
							<option>-- Select --</option>
							<% while(rs.next()) {%>
							<option value="<%=rs.getString("name")%>"><%=rs.getString("name")%></option>
							<% }%>
					</select> 
					<%
					    st.close();
    					con.close();
				}
				catch(Exception e)    
				{
   					out.println(e);
   					ex=e;
   					flag=false;
				}  

					%>
           </td>
           <br/>
        </tr>
        <br/>
        <tr>
              <th style= "text-align: right;"> </th>
              <td ><img src="images/user-icon.png" style="width: 100px; height: 100px"   name="image"></td>
          <br/><br/> <th style= "text-align: right;">Address&nbsp;&nbsp;&nbsp;<br/><br/><br/><br/><br/><br/></th>
           <td ><textarea rows="3" cols="30" name="ptaddress"></textarea>

           <br/>
              <br/><input type='image' height='50' width="130" src='images/submit.png'><br/></td>
        </tr>
         
     
       </table>
     </form>
     
           <%@include file="showPatientTable.jsp" %>
       
    </div>
            
            
           
<div>


</div>



 
            <div class="footer">


<div  style="padding:10px 5px 15px 20px;">
    © 2014 Scripting Logic - All Rights Reserved.
</div> 
</div>
    </body>
</html>
