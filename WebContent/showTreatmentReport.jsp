<%@ page contentType="application/pdf" %>

<%@ page trimDirectiveWhitespaces="true"%>

<%@ page import="net.sf.jasperreports.engine.*" %>
<%@ page import="java.io.File" %>
<%@ page import="java.io.FileInputStream" %>
<%@ page import="java.io.FileNotFoundException" %>
<%@ page import="java.io.InputStream" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.SQLException" %>
<%@page import="java.math.BigDecimal"%>
<%@page import="static com.dbcon.MyDriver.*"%>

<%
	BigDecimal id=null;
	BigDecimal tid = null;
	if(session!=null)
	{  
	 id = (BigDecimal) session.getAttribute("Clinic_Id");
	 tid= (BigDecimal) session.getAttribute("tid");
	}
    Connection conn=null;
   // System.out.println("tid = "+tid);
    try {
        //Connecting to the MySQL database
       Class.forName(DB_DRIVER);
		Connection con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS); 
        String queryString = "SELECT "+ 
	 		     "all_clinic.clinic_id AS all_clinic_clinic_id, " +  
	 		     "all_clinic.clinic_name AS all_clinic_clinic_name, " +
	 		     "all_clinic.clinic_address AS all_clinic_clinic_address, "+
	 		     "all_clinic.doctor_name AS all_clinic_doctor_name, "+
	 		     "all_clinic.email AS all_clinic_email, "+
	 		     "all_clinic.contact AS all_clinic_contact, "+
	 		     "medications.treatment_id AS medications_treatment_id, "+
	 		     "medications.medicine AS medications_medicine, "+
	 		     "medications.morning_pill AS medications_morning_pill, "+
	 		     "medications.afternoon_pill AS medications_afternoon_pill, "+
	 		     "medications.night_pill AS medications_night_pill, "+
	 		     "medications.total AS medications_total, "+
	 		     "medications.clinic_id AS medications_clinic_id, "+
	 		     "medications.medication_id AS medications_medication_id, "+
	 		     "treatment.treatment_id AS treatment_treatment_id, "+
	 		     "treatment.patient_name AS treatment_patient_name, "+
	 		     "treatment.patient_weight AS treatment_patient_weight, "+
	 		     "treatment.patient_pluse AS treatment_patient_pluse, "+
	 		     "treatment.treatment_date AS treatment_treatment_date, "+
	 		     "treatment.descripation AS treatment_descripation, "+
	 		     "treatment.fees AS treatment_fees, "+
	 		     "treatment.next_apt_visit AS treatment_next_apt_visit, "+
	 		     "treatment.clinic_id AS treatment_clinic_id, "+
	 		     "treatment.patient_bpmax AS treatment_patient_bpmax "+
	 		"FROM " +
	 		     "medications medications INNER JOIN all_clinic all_clinic ON medications.`clinic_id` = all_clinic.`clinic_id` "+
	 		     "INNER JOIN `treatment` treatment ON medications.`clinic_id` = treatment.`clinic_id` "+
	 		     "AND treatment.`treatment_id` = medications.`treatment_id` "+
	 		     "WHERE treatment.treatment_id = "+tid;
	 		     

	 	System.out.println(queryString);;

        Statement stmt = con.createStatement();
        ResultSet rset = stmt.executeQuery(queryString);
        JRResultSetDataSource jasperReports = new JRResultSetDataSource(rset);






        //Loading Jasper Report File from Local file system
        String jrxmlFile = session.getServletContext().getRealPath(request.getContextPath())+"/treatment.jrxml";
        InputStream input = new FileInputStream(new File(jrxmlFile));

        //Generating the report
        JasperReport jasperReport = JasperCompileManager.compileReport(input);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null, jasperReports);

        //Exporting the report as a PDF
        JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
    } catch (FileNotFoundException e) {
        e.printStackTrace();
    } catch (JRException e) {
        e.printStackTrace();
    } catch (ClassNotFoundException e) {
        e.printStackTrace();
    } catch (SQLException e) {
        e.printStackTrace();
    }
    finally {
        if(conn!=null){
            conn.close();
        }
    }

%>
