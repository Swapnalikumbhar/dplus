

<%@page import="java.math.BigDecimal"%>
<%@page import="static com.dbcon.MyDriver.*"%>
<%@page import="java.sql.*"%>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Item</title>
</head>
<body>

	<% 
    String stAddcat=request.getParameter("category");
     String stAddnam=request.getParameter("itemname");
     String stAddlevel=request.getParameter("level");
     
     String stDescription=request.getParameter("description");
     BigDecimal id=null;
     if(session!=null){  
    	 id = (BigDecimal) session.getAttribute("Clinic_Id");
    	 System.out.println("id = " + id);
     }
    
     
    //out.println("addItem");
    
    Exception ex=null;
    boolean flag=true;
   try
   {
    
     
	   Class.forName(DB_DRIVER);
	 		Connection con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS); 
    
    /*Class.forName("com.mysql.jdbc.Driver");
    Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/clinic","root","root"); */
    
    
    
    
    String sql= "insert into additem(category,itemname,itemquantity,clinic_id,reorderlevel,description)  values('"+stAddcat+"','"+stAddnam+"',0,"+ id +","+stAddlevel+",'"+stDescription+"')" ;    
	System.out.println(sql);   
    Statement st=con.createStatement();
	    st.executeUpdate(sql);
	    st.close();
	    con.close();
   }
   catch(Exception e)    
   {
       out.println(e);
       ex=e;
       flag=false;
   }    
  %>
	<script>
      
      if(<%=flag%>)
      {
          alert("Record inserted successfully");
          window.location= "addItem.jsp"
      }
      else
      {
          alert("Record not inserted"+"<%=ex%>")
          window.location= "addItem.jsp"
      }
  </script>


</body>
</html>