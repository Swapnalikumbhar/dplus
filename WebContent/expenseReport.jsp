<%-- 
    Document   : patient
    Created on : May 25, 2015, 12:36:15 AM
    Author     : amol
--%>

<%@page import="java.math.BigDecimal"%>
<%@page import="static com.dbcon.MyDriver.*"%>
<%@page import="java.sql.*"%>
<%@page  import="com.MyDate.*"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/tablescroll.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Doctor Plus</title>

<script src="js/jquery.min.js"></script>


<style>
.footer
 {
	height: 40px;
	width: 100%;
	color: white;
	background-color: #191919;
	margin-top: 25%;
	text-align: center;
}

.styleP {
	font-size: 50px;
	color: red;
}

.maincontainer {
	height: 80%;
	width: 90%;
	margin-left: 10%;
}

.maincontainertable {
	width: 85%;
	background-color: #79b7e7;
}

.medications {
	border: 2px;
	alignment-adjust: auto;
	margin-left: 10%;
	overflow: scroll;
	width: 600px;
	height: 180px;
}

body {
	background-color: #cccccc;
}

.endtable {
	width: 700px;
	height: 250px;
	margin-left: 5%;
}
</style>

     <link rel="stylesheet" href="css/jquery-ui.css">
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/jquery.min.js"></script>


<link href="jquery.datepick.package-5.0.0/jquery.datepick.css" rel="stylesheet">
<script src="js/jquery.min.js"></script>
<script src="jquery.datepick.package-5.0.0/jquery.plugin.js"></script>
<script src="jquery.datepick.package-5.0.0/jquery.datepick.js"></script>
<script>


$(function() {
	$('#fromentrydate').datepick();
        $('#inlineDatepicker').datepick({onSelect: showDate});
});

function showDate(date) {
	alert('The date chosen is ' + date);
	
}

</script>

<link href="jquery.datepick.package-5.0.0/jquery.datepick.css" rel="stylesheet">
<script src="js/jquery.min.js"></script>
<script src="jquery.datepick.package-5.0.0/jquery.plugin.js"></script>
<script src="jquery.datepick.package-5.0.0/jquery.datepick.js"></script>
<script>


$(function() {
	$('#toentrydate').datepick();
        $('#inlineDatepicker').datepick({onSelect: showDate});
});

function showDate(date) {
	alert('The date chosen is ' + date);
	
}

</script>

<script>

    $( "#other" ).click(function() {
  $( "#target" ).scroll();
});
</script>
</head>
<body>

	<%@include file="menu.jsp"%>
	
	 <%
if(session.getAttribute("User_Name")== null)
{
	response.sendRedirect("index.jsp");
}
%> 
	
<div class="maincontainer">
<div align="left">
	<label>Welcome <%=session.getAttribute("User_Name") %> </label>
   
    <h3>
			            <b>Expense Report</b>
		             </h3>
   
<form action="expenseReportAction.jsp" method="post"> 

 <table border="0" cellspacing="2" cellspading="1" align="center" width="700">
            
            
            <tr style=align: right; color: white">
			
              <th style="text-align: "right";">From Date&nbsp;&nbsp;&nbsp;</th>
			 <td><input type="text" style="width: 150px" id="fromentrydate"name="entrydateFrom">
			 
	
     <script>
             document.getElementById("fromentrydate").value = new Date().toJSON().slice(0,10) 
            
     </script></td>
			 
			 <th style="text-align: "right";">To Date&nbsp;&nbsp;&nbsp;</th>
			 <td><input type="text" style="width: 150px" id="toentrydate"name="entrydateTo">
			 
			  <script>
             document.getElementById("toentrydate").value = new Date().toJSON().slice(0,10) 
            
          </script></td>
			 
			 
			 <th style="text-align: left;" align="left"></th>
                   <td>
                   <input type='image' height='40' width="100" src='images/submit.png' align="left"><br />
                   </td>
                   
                   
                   

                   
			 </tr>
			 </table> 
</form>


	</div>
			
 
                     
	       


	
	<div class="footer">

		<div style="padding: 10px 5px 15px 20px;">© 2014 Scripting Logic
			- All Rights Reserved.</div>
	</div>
	

</html>
