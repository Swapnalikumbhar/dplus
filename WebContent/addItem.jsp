<%-- 
    Document   : patient
    Created on : May 25, 2015, 12:36:15 AM
    Author     : amol
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="static com.dbcon.MyDriver.*"%>
<%@page import="java.sql.*"%>
<%@page import="java.math.BigDecimal"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Doctor Plus</title>

<script src="js/jquery.min.js"></script>


<style>
.footer {
	height: 40px;
	width: 100%;
	color: white;
	background-color: #191919;
	margin-top: 12%;
	text-align: center;
}

.styleP {
	font-size: 50px;
	color: red;
}

.maincontainer {
	height: 70%px;
	width: 90%;
	margin-left: 10%;
}

.maincontainertable {
	width: 85%;
	background-color: #79b7e7;
}

.medications {
	border: 2px;
	alignment-adjust: auto;
	margin-left: 10%;
	overflow: scroll;
	width: 600px;
	height: 180px;
}

body {
	background-color: #cccccc;
}

.endtable {
	width: 700px;
	height: 250px;
	margin-left: 5%;
}
</style>

<!--        <link rel="stylesheet" href="css/jquery-ui.css">
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/jquery.min.js"></script>-->

<script>
    $( "#other" ).click(function() {
  $( "#target" ).scroll();
});
</script>

<script>
function validate()
{
	
var categoryname=document.getElementById("categoryid").value;
var itemtype=document.getElementById("itemid").value;
var reorderlevel=document.getElementById("levelid").value;
//alert(categoryname);
if(categoryname=="-- Select --")
	{
	alert("please select category ");
	return false;
	}
else if(itemtype=="")
{
alert("please Enter Item Type ");
return false;
}
else if(reorderlevel=="")
	{
	alert("please Enter reorder level ");
	return false;
	}
else{
	return true;
}
}
</script>


</head>
<body>

	<%@include file="menu.jsp"%>

<%
if(session.getAttribute("User_Name")== null)
{
	response.sendRedirect("index.jsp");
}
%>

	<div class="maincontainer">
	<div align="right">
	<label>Welcome <%=session.getAttribute("User_Name") %> </label>
    </div>
	
		<h3>
			<b>Add Item</b>
		</h3>
		<br />

		<form action="addItemAddAction.jsp" method="post" onsubmit="return validate()">

			<table class="endtable">

				<tr>

					<th style="text-align: right;">Category&nbsp;&nbsp;&nbsp;</th>
					<td>
						<%

						BigDecimal id=null;
			        	 if(session!=null)
			        	 {  
			        	 	 id = (BigDecimal) session.getAttribute("Clinic_Id");
			        	 	 System.out.println("id = " + id);
			        	 }

Exception ex=null;
boolean flag=true;
try
{

   String sql= "select * from category where clinic_id="+id ;    
	Class.forName(DB_DRIVER);
	Connection con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS);  
  
    		   Statement st=con.createStatement();
    		    ResultSet rs=st.executeQuery(sql);
	%> <select name="category" style="width: 160px;" id="categoryid">

							<option>-- Select --</option>

							<% while(rs.next()) {%>

							<option value="<%=rs.getBigDecimal("id")%>"><%=rs.getString("itemcategory")%></option>
							<% }%>

					</select> <%
 
    st.close();
    con.close();
}
catch(Exception e)    
{
   out.println(e);
   ex=e;
   flag=false;
}  

%>

					</td>
				</tr>



				<tr>

					<th style="text-align: right;">Item type&nbsp;&nbsp;&nbsp;</th>
					<td><input type="text" style="width: 160px" name="itemname" id="itemid"></td>
				</tr>
				
				<tr>
					<th style="text-align: right;">Reorder Level&nbsp;&nbsp;&nbsp;</th>
					<td><input type="number" style="width: 160px" name="level"id="levelid"></td>
				</tr>
				
				

                <tr> 
                    <th style= "text-align: right;"> Description&nbsp;&nbsp;&nbsp;</th>
                    <td><textarea name="description" id="description" rows="3" cols="23"></textarea></td>
               </tr>

				<tr>
					<th style="text-align: right;"></th>

					<td><input type='image' height='50' width="130"
						src='images/submit.png'><br /></td>
				</tr>

				

			</table>
		
		</form>
		
		            <center>
		            <%@include file="showAddItem.jsp" %>
	             
		           </center>
		
	</div>
	<div class="footer">

		<div style="padding: 10px 5px 15px 20px;">© 2014 Scripting Logic
			- All Rights Reserved.</div>
</body>
</html>
