<%-- 
    Document   : patient
    Created on : May 25, 2015, 12:36:15 AM
    Author     : amol
--%>

<%@page import="java.math.BigDecimal"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="static com.dbcon.MyDriver.*"%>
<%@page import="java.sql.*"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/tablescroll.css">
<title>Doctor Plus</title>

<script src="js/jquery.min.js"></script>


<style>
.footer {
	height: 40px;
	width: 100%;
	color: white;
	background-color: #191919;
	margin-top: 12%;
	text-align: center;
}

.styleP {
	font-size: 50px;
	color: red;
}

.maincontainer {
	height: 400px;
	width: 90%;
	margin-left: 10%;
}

.maincontainertable {
	width: 85%;
	background-color: #79b7e7;
}

.medications {
	border: 2px;
	alignment-adjust: auto;
	margin-left: 10%;
	overflow: scroll;
	width: 600px;
	height: 180px;
}

body {
	background-color: #cccccc;
}

.endtable {
	width: 700px;
	height: 250px;
	margin-left: 5%;
}
</style>

<!--        <link rel="stylesheet" href="css/jquery-ui.css">
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/jquery.min.js"></script>-->

<script>
    $( "#other" ).click(function() {
  $( "#target" ).scroll();
});
</script>
</head>
<body>


	<%@include file="menu.jsp"%>
	
	<%
if(session.getAttribute("User_Name")== null)
{
	response.sendRedirect("index.jsp");
}
%>

	<div class="maincontainer">
	<div align="right">
	<label>Welcome <%=session.getAttribute("User_Name") %> </label>
    </div>
	 <center>
		<h3>
			<b>View Item Stock</b>
		</h3>
		<br />

		<form action="viewMaterialAddAction.jsp">

			<table class="fixed_headers" style="width:60%">
			<thead>

				<tr>
					<!-- <td>Id</td> -->
					<td>category</td>
					<td>Item Name</td>
					<td>Quantity</td>



				</tr>
		</thead>
		<tbody>

				<%
             try
   {
    
            	 BigDecimal id=null;
            	 if(session!=null){  
            	 	 id = (BigDecimal) session.getAttribute("Clinic_Id");
            	 	 System.out.println("id = " + id);
            	 }
       String sql= "select * from additem where clinic_id="+id;
              
    
       Class.forName(DB_DRIVER);
     		Connection con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS); 
    Statement st=con.createStatement();
    ResultSet rs = st.executeQuery(sql);
    
    while(rs.next())
   
    {%>

				<tr>
					<%-- <td><%=rs.getInt("id")%></td> --%>



					<td>
						<% 
                
                
                int catId = Integer.parseInt(""+rs.getBigDecimal("category"));
                String sqlCat = "select itemcategory from category where id="+catId+" and clinic_id="+id;
                Statement st2=con.createStatement();
                ResultSet rs2 = st2.executeQuery(sqlCat);
                rs2.next();
                out.println(rs2.getString("itemcategory"));
               %>


					</td>



					<td><%=rs.getString("itemname")%></td>
					<td><%=rs.getInt("itemquantity")%></td>



				</tr>

				<%}
    
    
    st.close();
    con.close();
   }
   catch(Exception e)
    
   {
       out.println(e);
      
   }
    
  %>
  			</center>
				</tbody>
			</table>

		</form>
	</div>
	<div class="footer">

		<div style="padding: 10px 5px 15px 20px;">© 2014 Scripting Logic
	         		- All Rights Reserved.</div>
	
</body>
</html>
