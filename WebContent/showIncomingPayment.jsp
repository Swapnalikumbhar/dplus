
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="java.util.Date"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="static com.dbcon.MyDriver.*"%>


<html>
<head>
<link rel="stylesheet" type="text/css" href="css/tablescroll.css">
</head>
<body>
	  
		
		<br />

		

			<table  style="width:80%">
			<thead>

				<tr style="background-color:black;color:white">
					
					<td>Date</td>
					<td>Payment Type</td>
					<td>Paid To Party</td>
					<td>Payment Mode</td>
					<td>Amount</td>
					<td>Cheque/DD no</td>
					<td>Information</td>
					<td></td>
				</tr>

</thead>
 <tbody>
				<%
             try
   {
            	 BigDecimal id=null;
            	 if(session!=null){  
            	 	 id = (BigDecimal) session.getAttribute("Clinic_Id");
            	 	 //System.out.println("id = " + id);
            	 }

            String sql= "select * from incomingpayment where clinic_id="+id;
              
    
            Class.forName(DB_DRIVER);
     		Connection con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS); 
            Statement st=con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            String exenseStr="";
            int i=0;
            String color="";
            while(rs.next())
   
           { i++;
           if (i%2==0)
           {
        	   color="gray";
           }
           else
           {
        	   color="white";
           }
             
           
           %>	
				<tr style="background-color:<%=color%>;">
					<td><%=rs.getString("entrydate")%></td>
					<td>
					  <%
                 String catId =""+rs.getBigDecimal("paymenttype");
                String sqlCat = "select expensetype from expense where id="+catId;
                Statement st2=con.createStatement();
                ResultSet rs2 = st2.executeQuery(sqlCat);
                rs2.next(); 
            	 exenseStr=rs2.getString("expensetype");
                 out.println(exenseStr);
               %> 
               </td>
					<td><%=rs.getString("paytoprty")%></td>
					<td><%=rs.getString("paymode")%></td>
					<td><%=rs.getString("amount")%></td>
					<td><%=rs.getString("ddno")%></td>
					<td><%=rs.getString("information")%></td>
				<td>			
	<form action="incomingPaymentEditAction.jsp" method="post" >				
 
<input type="hidden" name="idhid" value="<%=rs.getInt("id")%>">
<input type="hidden" name="showdatehid" value="<%=rs.getDate("entrydate")%>">
<input type="hidden" name="paymenthid" value="<%=rs2.getString("expensetype")%>">
<input type="hidden" name="paytoprtyhid" value="<%=rs.getString("paytoprty")%>">
<input type="hidden" name="paymodehid" value="<%=rs.getString("paymode")%>">
<input type="hidden" name="amounthid" value="<%=rs.getString("amount")%>">
<input type="hidden" name="ddnohid" value="<%=rs.getString("ddno")%>">
<input type="hidden" name="informationhid" value="<%=rs.getString("information")%>">
<input type="image" src="images/edit.png" height="15px"  width="15px" >

</form>		
		
<%}
    
    
    st.close();
    con.close();
   }
   catch(Exception e)
    
   {
       out.println(e);
      
   }
    
  %>




				</tr>
        </tbody>
			</table>
		

				
        
 </body>
</html>