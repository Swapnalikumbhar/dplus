<%-- 
    Document   : patient
    Created on : May 25, 2015, 12:36:15 AM
    Author     : amol
--%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Doctor Plus</title>

<script src="js/jquery.min.js"></script>


<style>
.footer {
	height: 40px;
	width: 100%;
	color: white;
	background-color: #191919;
	margin-top: 12%;
	text-align: center;
}

.styleP {
	font-size: 50px;
	color: red;
}

.maincontainer {
	height: 70%px;
	width: 90%;
	margin-left: 10%;
}

.maincontainertable {
	width: 85%;
	background-color: #79b7e7;
}

.medications {
	border: 2px;
	alignment-adjust: auto;
	margin-left: 10%;
	overflow: scroll;
	width: 600px;
	height: 180px;
}

body {
	background-color: #cccccc;
}

.endtable {
	width: 700px;
	height: 250px;
	margin-left: 5%;
}
</style>

<!--        <link rel="stylesheet" href="css/jquery-ui.css">
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/jquery.min.js"></script>-->

<link href="jquery.datepick.package-5.0.0/jquery.datepick.css" rel="stylesheet">
<script src="js/jquery.min.js"></script>
<script src="jquery.datepick.package-5.0.0/jquery.plugin.js"></script>
<script src="jquery.datepick.package-5.0.0/jquery.datepick.js"></script>
<script>
$(function() {
	$('#entrydate').datepick();
        $('#inlineDatepicker').datepick({onSelect: showDate});
});

function showDate(date) {
	alert('The date chosen is ' + date);
	
}

</script>
 

<script>
    $( "#other" ).click(function() {
  $( "#target" ).scroll();
});
</script>
</head>
<body>

	<%@include file="menu.jsp"%>
	
	
<%
if(session.getAttribute("User_Name")== null)
{
	response.sendRedirect("index.jsp");
}
%>

	<div class="maincontainer">
	<div align="right">
	<label>Welcome <%=session.getAttribute("User_Name") %> </label>
    </div>

<% 


 String stDate=request.getParameter("showdatehid");// this is in the form of yyyy-MM-dd (string)
 //out.println("date from textbox :"+stDate);
 
 SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");

 SimpleDateFormat sd = new SimpleDateFormat("MM/dd/yyyy");
 
Date currDate = new Date();
Date curFormatDate = new Date();
curFormatDate = myFormat.parse(stDate);

 //out.println("Formatted date from textbox :"+curFormatDate);
String currDateFormatStr = sd.format(curFormatDate);
// out.println("date String:"+currDateFormatStr);
%>

		<form action="expenseMasterUpdateAction.jsp" method="post">
		<input type="hidden" name="id" value="<%=request.getParameter("idhid")%>">
			
		<table border="0" cellspacing="2" cellspading="1" align="center" width="700">
            
            
            <tr style=align: right; color: white">
			
              <th style="text-align: "right";">Date&nbsp;&nbsp;&nbsp;</th>
			 <td><input type="text" style="width: 150px" 
			 name="showdate" id="entrydate" value="<%=currDateFormatStr/* show the converted date  MM/dd/yyyy*/%>"></td>
				
				<th style="text-align: "right";">Expense No&nbsp;&nbsp;&nbsp;</th>
			   <td><input type="Number" style="width: 150px"
			   name="expenseno" value="<%=request.getParameter("expensenohid")%>"></td>
				</tr>
				</table>
		
			<div>
				<h3>
			   <h2><b>Expense Details</b></h2>
		       </h3>
		       <br />
		        </div>
				<table class="endtable">
      

				<tr>

					<th style="text-align: right;">Expense Type&nbsp;&nbsp;&nbsp;</th>
					<td>
					<%

Exception ex=null;
boolean flag=true;
try
{

     String sql= "select * from expense where clinic_id";    
	Class.forName(DB_DRIVER);
	Connection con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS);  
    Statement st=con.createStatement();
    		    ResultSet rs=st.executeQuery(sql);
	%> <select name="expensetype" style="width: 150px;" id="expenseid">

							<option><%=request.getParameter("expensehid")%></option>

							<% while(rs.next()) {%>

							<option value="<%=rs.getBigDecimal("id")%>"><%=rs.getString("expensetype")%></option>
							<%}%>

					</select> <%
 
    st.close();
    con.close();
}
catch(Exception e)    
{
   out.println(e);
   ex=e;
   flag=false;
}  

%>


</td>
						
						<th style="text-align: right;">Paid To Party&nbsp;&nbsp;&nbsp;</th>
					<td><input type="text" style="width: 150px"
					name="paytoprty"  value="<%=request.getParameter("paytoprtyhid")%>"></td>
				</tr>
				<tr>

					<th style="text-align: right;">Payment Mode&nbsp;&nbsp;&nbsp;</th>
					<td><select name="paymode" style="width: 150px"">
					    <option value="Select"><%=request.getParameter("paymodehid")%></option>
					    <option value="Cheque">Cheque</option>
					    <option value="Cash">Cash</option>  
					    </select>
					    
					    
					<th style="text-align: right;">Amount&nbsp;&nbsp;&nbsp;</th>
					<td><input type="Number" style="width: 150px" 
					name="amount" value="<%=request.getParameter("amounthid")%>"></td>
					
					</td>
					<tr>
					<th style="text-align: right;">Cheque/DD No&nbsp;&nbsp;&nbsp;</th>
					<td><input type="text" style="width: 150px" 
					 name="ddno" value="<%=request.getParameter("ddnohid")%>"></td>
					
					<th style="text-align: right;">Additional Information&nbsp;&nbsp;&nbsp;</th>
					<td><textarea name="information" value=""
					rows="4" cols="21"><%=request.getParameter("informationhid")%></textarea></td>
					
				</tr>
				


				<tr>
					<th style="text-align: left;" align="left"></th>
                   <td>
                   <input type='image' height='50' width="130" src='images/submit.png' align="left"><br />
                   </td>
                   
                   <th style="text-align: left;" align="left"></th>
                   <td>
                   <input type='image' height='50' width="130" src='images/cancel1.png' align="left"><br />
                   </td>



			</table>
		</form>
		<center>
		<%@include file="showExpenseMaster.jsp"%>
		</center>
	</div>
	<div class="footer">
	
	

		<div style="padding: 10px 5px 15px 20px;">© 2014 Scripting Logic
			- All Rights Reserved.</div>
</body>
</html>
