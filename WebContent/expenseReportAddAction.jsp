<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="static com.dbcon.MyDriver.*"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Date"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Expense Report</title>
</head>
<body>

	<% 
    String stExpenseReport=request.getParameter("expensemaster");
	String stExpenseDate=request.getParameter("entrydate");
	String stamount=request.getParameter("totalamount");
	String stpayment=request.getParameter("totalpayment");
	String stdescription=request.getParameter("description");
	
	SimpleDateFormat sd = new SimpleDateFormat("MM/dd/yyyy");
    
    SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
    
 Date currDate = new Date();
    
    
    Date curFormatDate = new Date();
    curFormatDate = sd.parse(stExpenseDate);
    //out.println("Formatted date from textbox :"+curFormatDate);
    String currDateFormatStr = myFormat.format(curFormatDate);
    //out.println("date String:"+currDateFormatStr);
    
   //out.println("expenseType");
    BigDecimal id=null;
if(session!=null){  
	 id = (BigDecimal) session.getAttribute("Clinic_Id");
	 //System.out.println("id = " + id);
}

    Exception ex=null;
    boolean flag=true;
   try
   {
    
       String sql= "insert into expensereport(expensetype,expensedate,expensetotal,expensepayamount,description,clinic_id)  values('"+stExpenseReport+"','"+currDateFormatStr+"',"+stamount+","+stpayment+",'"+stdescription+"',"+id+")" ;    
       Class.forName(DB_DRIVER);
   	Connection con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS); 
    
    /*Class.forName("com.mysql.jdbc.Driver");
    Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/clinic","root","root"); */
    
	    Statement st=con.createStatement();
	    st.executeUpdate(sql);
	    st.close();
	    con.close();
   }
   catch(Exception e)    
   {
       out.println(e);
       ex=e;
       flag=false;
   }    
  %>
	<script>
      
      if(<%=flag%>)
      {
          alert("Record inserted successfully");
          window.location= "expenseReport.jsp"
      }
      else
      {
          alert("Record not inserted"+"<%=ex%>")
          window.location= "expenseReport.jsp"
      }
  </script>


</body>
</html>