<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="static com.dbcon.MyDriver.*"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="java.util.Date"%>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Purchase</title>
</head>
<body>

     <% 
    String stAddcat=request.getParameter("category");
     String stAddnam=request.getParameter("Item");
     
     String stAddunit=request.getParameter("unitprice");
     String stAddquan=request.getParameter("quantity");
     String stAddtotal=request.getParameter("totalcost");
     
     String stDescription=request.getParameter("description");
     
     String stDate=request.getParameter("entrydate");
     //out.println("date from textbox :"+stDate);
     SimpleDateFormat sd = new SimpleDateFormat("MM/dd/yyyy");
     
     SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
      
    Date currDate = new Date();
    
    
    Date curFormatDate = new Date();
    curFormatDate = sd.parse(stDate);
    //out.println("Formatted date from textbox :"+curFormatDate);
    String currDateFormatStr = myFormat.format(curFormatDate);
    //out.println("date String:"+currDateFormatStr);
    
    
    //out.println("additem");
BigDecimal id=null;
if(session!=null){  
	 id = (BigDecimal) session.getAttribute("Clinic_Id");
	 System.out.println("id = " + id);
}
    
    Exception ex=null;
    boolean flag=true;
    
   // int row=stdRecords.getSelectedRow();
    
   // Object idObj=  stdRecords.getValueAt(row, 0);
   try
   {
    
     
	   Class.forName(DB_DRIVER);
   	Connection con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS);  
   	
    /*Class.forName("com.mysql.jdbc.Driver");
    Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/clinic","root","root"); */
    
    
    
    
    String sqlInsert="insert into purchase(category,itemtype,unitprice,quantity,totalcost,clinic_id,entrydate,description) values("+stAddcat+","+stAddnam+","+stAddunit+","+stAddquan+","+stAddtotal+","+id+",'"+currDateFormatStr+"','"+stDescription+"')";   
    //out.println(sqlInsert);
    String sqlUpdateQty="update additem set itemquantity=itemquantity+"+stAddquan+" where id="+stAddnam+" and clinic_id="+id;
    
    Statement st=con.createStatement();
    st.executeUpdate(sqlInsert);
    st.executeUpdate(sqlUpdateQty);
	   // String sql="select * from additem";
       // ResultSet rs=st.executeQuery(sql);
       // stdRecords.setModel(DbUtils.resultSetToTableModel(rs));
	    st.close();
	    con.close();
   }
   catch(Exception e)    
   {
       out.println(e);
       ex=e;
       flag=false;
   }    
  %>  
  <script>
      
      if(<%=flag%>)
      {
          alert("Record inserted successfully");
          window.location= "purchase.jsp"
      }
      else
      {
          alert("Record not inserted"+"<%=ex%>")
         window.location= "purchase.jsp"
      }
  </script>


</body>
</html>