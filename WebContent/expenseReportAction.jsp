<%-- 
    Document   : patient
    Created on : May 25, 2015, 12:36:15 AM
    Author     : amol
--%>

<%@page import="java.math.BigDecimal"%>
<%@page import="static com.dbcon.MyDriver.*"%>
<%@page import="java.sql.*"%>
<%@page  import="com.MyDate.*"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/tablescroll.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Doctor Plus</title>

<script src="js/jquery.min.js"></script>


<style>
.footer
 {
	height: 40px;
	width: 100%;
	color: white;
	background-color: #191919;
	margin-top: 25%;
	text-align: center;
}

.styleP {
	font-size: 50px;
	color: red;
}

.maincontainer {
	height: 80%;
	width: 90%;
	margin-left: 10%;
}

.maincontainertable {
	width: 85%;
	background-color: #79b7e7;
}

.medications {
	border: 2px;
	alignment-adjust: auto;
	margin-left: 10%;
	overflow: scroll;
	width: 600px;
	height: 180px;
}

body {
	background-color: #cccccc;
}

.endtable {
	width: 700px;
	height: 250px;
	margin-left: 5%;
}
</style>

     <link rel="stylesheet" href="css/jquery-ui.css">
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/jquery.min.js"></script>


<link href="jquery.datepick.package-5.0.0/jquery.datepick.css" rel="stylesheet">
<script src="js/jquery.min.js"></script>
<script src="jquery.datepick.package-5.0.0/jquery.plugin.js"></script>
<script src="jquery.datepick.package-5.0.0/jquery.datepick.js"></script>
<script>


$(function() {
	$('#fromentrydate').datepick();
        $('#inlineDatepicker').datepick({onSelect: showDate});
});

function showDate(date) {
	alert('The date chosen is ' + date);
	
}

</script>

<link href="jquery.datepick.package-5.0.0/jquery.datepick.css" rel="stylesheet">
<script src="js/jquery.min.js"></script>
<script src="jquery.datepick.package-5.0.0/jquery.plugin.js"></script>
<script src="jquery.datepick.package-5.0.0/jquery.datepick.js"></script>
<script>


$(function() {
	$('#toentrydate').datepick();
        $('#inlineDatepicker').datepick({onSelect: showDate});
});

function showDate(date) {
	alert('The date chosen is ' + date);
	
}

</script>

<script>

    $( "#other" ).click(function() {
  $( "#target" ).scroll();
});
</script>
</head>
<body>

	<%@include file="menu.jsp"%>
	
	 <%
if(session.getAttribute("User_Name")== null)
{
	response.sendRedirect("index.jsp");
}
%> 
	
<div class="maincontainer">
<div align="left">
	<label>Welcome <%=session.getAttribute("User_Name") %> </label>
  <h3>
			            <b>Expense Report</b>
		             </h3>  
<form action="expenseReportAction.jsp" method="post"> 

 <table border="0" cellspacing="2" cellspading="1" align="center" width="700">
            
            
            <tr style=align: right; color: white">
			
              <th style="text-align: "right";">From Date&nbsp;&nbsp;&nbsp;</th>
			 <td><input type="text" style="width: 150px" id="fromentrydate"name="entrydateFrom">
			 
	
  </td>
			 
			 <th style="text-align: "right";">To Date&nbsp;&nbsp;&nbsp;</th>
			 <td><input type="text" style="width: 150px" id="toentrydate"name="entrydateTo">
			 
			</td>
			 
			 
			 <th style="text-align: left;" align="left"></th>
                   <td>
                   <input type='image' height='40' width="100" src='images/submit.png' align="left"><br />
                   </td>
                   
                   
                   

                   
			 </tr>
			 </table> 
</form>


	</di>
			
 
                    
	         	<br />
              </div>
			<table class="fixed_headers" style="width:100%">
			<thead>

				<tr>
				
					<td>Expense No</td>
					<td>Expense Type</td>
					<td>Paid To Party</td>
					<td>Payment Mode</td>
					<td>Amount</td>
					<td>Cheque/DD no</td>
					<td>Additional Information</td>
					<td>
					</tr>

</thead>

<tbody>


				<%
				
	            	
				
             try
              {
            	 BigDecimal id=null;
            	 if(session!=null)
            	 {  
            	 	 id = (BigDecimal) session.getAttribute("Clinic_Id");
            	 	 System.out.println("id = " + id);
            	 } 
            	 
            	 String fromDate=DateUtility.dateConvertion(request.getParameter("entrydateFrom"));
            	 
            	
            	 String toDate=DateUtility.dateConvertion(request.getParameter("entrydateTo"));
            	 
       String sql= "select * from expensedetails where entrydate BETWEEN '"+fromDate+"' AND '"+toDate+"' and Clinic_Id="+id;
    //     out.println(sql) ;    
    
         Class.forName(DB_DRIVER);
		 Connection con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS); 
         Statement st=con.createStatement();
         ResultSet rs = st.executeQuery(sql);
         String exenseStr="";
while(rs.next())
             
{%>
					
				<tr>
					
					<td><%=rs.getString("expenseno")%></td>
					<td>
					<%
                 String catId =""+rs.getBigDecimal("expensetype");
                String sqlCat = "select expensetype from expense where id="+catId;
                Statement st2=con.createStatement();
                ResultSet rs2 = st2.executeQuery(sqlCat);
                rs2.next(); 
            	 exenseStr=rs2.getString("expensetype");
                 out.println(exenseStr);
                 %></td>
					<%-- <td><%=rs.getString("expensetype")%></td> --%>
					<td><%=rs.getString("paytoprty")%></td>
					<td><%=rs.getString("paymode")%></td>
					<td><%=rs.getString("amount")%></td>
					<td><%=rs.getString("ddno")%></td>
					 <td><%=rs.getString("information")%></td> 
					
				</tr>
 
				<%}
    
    
    st.close();
    con.close();
   }
   catch(Exception e)
    
   {
       out.println(e);
      
   }
    
  
  %>
					
		</tbody>

			</table>
		


	
	<div class="footer">

		<div style="padding: 10px 5px 15px 20px;">© 2014 Scripting Logic
			- All Rights Reserved.</div>
	</div>
	

</html>
