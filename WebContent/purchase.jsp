<%-- 
    Document   : patient
    Created on : May 25, 2015, 12:36:15 AM
    Author     : amol
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="static com.dbcon.MyDriver.*"%>
<%@page import="java.sql.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Doctor Plus</title>

<script src="js/jquery.min.js"></script>

 <script language="javascript" type="text/javascript">  
            var xmlHttp;             
            
            function showState(catid)
            {
                if (typeof XMLHttpRequest !== "undefined")
                {
                  xmlHttp= new XMLHttpRequest();
                }
                var url="categoryWiseItems.jsp";
                
                url +="?category=" +catid;
                //url1 +="?email1=" +catid;
                
                xmlHttp.onreadystatechange = stateChange;
                xmlHttp.open("GET", url, true);
               // xmlHttp.open("GET", url1, true);
                xmlHttp.send(null);
            }
            function stateChange()
            {   
                if (xmlHttp.readyState===4 || xmlHttp.readyState==="complete")
                {   
                    document.getElementById("item").innerHTML=xmlHttp.responseText;
                  //  document.getElementById("Email").innerHTML=xmlHttp.responseText;
                }    
            }
          </script>




        <style>
            .footer {
        height :40px;
        width : 100%;
        color : white;
        background-color: #191919;
        margin-top: 12%;
        text-align: center;

        }
.styleP{
        font-size:50px;
        color : red;
        }
        .maincontainer{
            height:300px;
            width: 90%;
            margin-left: 10%;
           
           
            
        }
        .maincontainertable{
         
            width: 85%;
            background-color: #79b7e7;
            
            
        }
       
        .medications{
            
            border: 2px;
            alignment-adjust: auto;
            margin-left: 10%;
            overflow: scroll; 
            width : 600px;
            height: 180px;
                
        }
        body {
  
    background-color: #cccccc;
}
.endtable{
    width: 700px;
     height: 250px ;
    margin-left: 5%;
}

        </style>
        
<!--        <link rel="stylesheet" href="css/jquery-ui.css">
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/jquery.min.js"></script>-->
  
<script>
    $( "#other" ).click(function() {
  $( "#target" ).scroll();
});
</script>

<script type="text/javascript">
function total()
{
var a = document.getElementById("unitprice");
var b = document.getElementById("quantity");

if(a.value=="")
{
a.value = 0;
}
if(b.value=="")
{
b.value = 1;
}


var d = a.value * b.value;
document.getElementById("totalcost").value=d;
}
</script>




<link href="jquery.datepick.package-5.0.0/jquery.datepick.css" rel="stylesheet">
<script src="js/jquery.min.js"></script>
<script src="jquery.datepick.package-5.0.0/jquery.plugin.js"></script>
<script src="jquery.datepick.package-5.0.0/jquery.datepick.js"></script>
<script>
$(function() {
	$('#entrydate').datepick();
        $('#inlineDatepicker').datepick({onSelect: showDate});
});

function showDate(date) {
	alert('The date chosen is ' + date);
	
}

</script>
 
<script>
    $( "#other" ).click(function() {
  $( "#target" ).scroll();
  
  
});
    
    
   
    
    
    
</script>


    </head>
    <body  >
        
        <%@include file="menu.jsp"%>
        
        
        <%
if(session.getAttribute("User_Name")== null)
{
	response.sendRedirect("index.jsp");
}
%>
        
        <div class="maincontainer">
        <h3><b>Inword/Purchase</b></h3>
        <br/>
       
  <form action="purchaseAddAction.jsp"  method="post">     
        
 <table class="endtable">
            
            
          
                
                <tr style="height: 200 px;">
             
           <th style= "text-align: right;">Category&nbsp;&nbsp;&nbsp;</th>
           <td >

<%

Exception ex=null;
boolean flag=true;
try
{

   String sql= "select * from category" ;    
	Class.forName(DB_DRIVER);
	Connection con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS);  
  
    		   Statement st=con.createStatement();
    		    ResultSet rs=st.executeQuery(sql);
	%> <select name="category" style="width: 160px;">

							<option>-- Select --</option>

							<% while(rs.next()) {%>

							<option value="<%=rs.getBigDecimal

("id")%>"><%=rs.getString("itemcategory")%></option>
							<% }%>

					</select> <%
 
    st.close();
    con.close();
}
catch(Exception e)    
{
   out.println(e);
   ex=e;
   flag=false;
}  

%>

					</td>
				</tr>

				<tr>

					<th style="text-align: 

right;">Item&nbsp;&nbsp;&nbsp;</th>
					<td>
						<%


try
{

   String sql= "select * from addItem" ;    
	Class.forName(DB_DRIVER);
	Connection con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS);  
  
    		   Statement st=con.createStatement();
    		    ResultSet rs=st.executeQuery(sql);
	%> <select name="Item" style="width: 160px;">

		<option>-- Select --</option>

		<% while(rs.next()) {%>

	<option value="<%=rs.getBigDecimal("id")%>">
              <%=rs.getString("itemname")%></option>
				                <% }%>

					</select> <%
 
    st.close();
    con.close();
}
catch(Exception e)    
{
   out.println(e);
   ex=e;
   flag=false;
}  

%>


</td>
</tr>
             
                <tr>
                <th style= "text-align: right;">Date&nbsp;&nbsp;&nbsp; </th>
                <td><input type="text" style="width: 160px" id="entrydate"  name="entrydate" ></td><td></td>
                </tr>
                
                <tr>
                <th style= "text-align: right;"> Unit Price&nbsp;&nbsp;&nbsp;</th>
                <td><input type="text" name="unitprice" id="unitprice" size="20"  onkeyup="total()"></td><td></td>
                </tr>
               
                <tr> 
                <th style= "text-align: right;"> Quantity&nbsp;&nbsp;&nbsp;</th>
                <td><input type="text" name="quantity" id="quantity" size="20"   onkeyup="total()"></td><td></td>
                </tr>
       
                <tr>
                <th style= "text-align: right;"> total Cost&nbsp;&nbsp;&nbsp;</th>
                <td><input type="text" name="totalcost" id="totalcost" size="20"  onkeyup="total()"></td><td></td>
                </tr>
              
               <tr>
               <th style= "text-align: right;"> Description&nbsp;&nbsp;&nbsp;</th>
               <td><textarea name="description" id="description" rows="3" cols="23"></textarea></td><td></td>
               </tr>
              
                <tr>
                <br><td ></td><td><<input type='image' height='40' width="100" 

               src='images/submit.png'>&nbsp;&nbsp;&nbsp;&nbsp;</td></br>
                </tr>
                
              
               
               
                 
        </table>
             
       </form>
    </div>
            <div class="footer">

                <div  style="padding:10px 5px 15px 20px;">
                © 2014 Scripting Logic - All Rights Reserved.
</div> 
    </body>
</html>