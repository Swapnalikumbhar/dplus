<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="static com.dbcon.MyDriver.*"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<%

String idStr = request.getParameter("id");

String showdateStr=request.getParameter("showdate");
out.println("date from textbox :"+showdateStr);
SimpleDateFormat sd = new SimpleDateFormat("MM/dd/yyyy");
SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
Date currDate = new Date();
Date curFormatDate = new Date();
curFormatDate = sd.parse(showdateStr);
out.println("Formatted date from textbox :"+curFormatDate);
String currDateFormatStr = myFormat.format(curFormatDate);
out.println("date String:"+currDateFormatStr);



String expensenoStr=request.getParameter("expenseno");



String expenseStr=request.getParameter("expensetype");
  



String paytoprtyStr=request.getParameter("paytoprty");
String paymodeStr=request.getParameter("paymode");
String amountStr=request.getParameter("amount");
String ddnoStr=request.getParameter("ddno");
String informationStr=request.getParameter("information");


BigDecimal id=null;
if(session!=null){  
	 id = (BigDecimal) session.getAttribute("Clinic_Id");
	// System.out.println("id = " + id);
}


Exception ex=null;
boolean flag=true;

try
{
 
  
	   Class.forName(DB_DRIVER);
		Connection con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS); 
       String sqlUpdate="update expensedetails set entrydate='"+currDateFormatStr+"',expenseno="+expensenoStr+",expensetype='"+expenseStr+"',paytoprty='"+paytoprtyStr+"',paymode='"+paymodeStr+"',amount="+amountStr+",ddno="+ddnoStr+",information='"+informationStr+"' where id="+idStr+" and clinic_id="+id;
	   out.println(sqlUpdate);
       Statement st=con.createStatement();
	    st.executeUpdate(sqlUpdate);
	   
	    st.close();
	    con.close();
}
catch(Exception e)    
{
    out.println(e);
    ex=e;
    flag=false;
}    

%>


<script>
      
      if(<%=flag%>)
      {
          alert("Record Updated successfully");
          window.location= "expenseMaster.jsp"
      }
      else
      {
          alert("Record not Updated"+"<%=ex%>")
          window.location= "expenseMaster.jsp"
      }
  </script>

</body>
</html>