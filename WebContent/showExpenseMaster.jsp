

<%@page import="java.math.BigDecimal"%>
<%@page import="static com.dbcon.MyDriver.*"%>
<%@page import="java.sql.*"%>

<html>
<head>
<link rel="stylesheet" type="text/css" href="css/tablescroll.css">
</head>
<body>
	  
		
		<br />

		

			<table style="width:100%">
			<thead>

				<tr style="background-color:black;color:white">
					
					<td>Date</td>
					
					<td>Expense Type</td>
					
					<td>Paid To Party</td>
					
					<td>Payment Mode</td>
					<td>Amount</td>
					<td>Cheque/DD no</td>
					<td>Additional Information</td>
					<td></td>
					



				</tr>

</thead>
 <tbody>
				<%
             try
   {
            	 BigDecimal id=null;
            	 if(session!=null){  
            	 	 id = (BigDecimal) session.getAttribute("Clinic_Id");
            	 	 //System.out.println("id = " + id);
            	 }

            String sql= "select * from expensedetails where clinic_id="+id;
              
    
            Class.forName(DB_DRIVER);
     		Connection con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS); 
            Statement st=con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            String exenseStr="";
            String paymodeStr="";
            int i=0;
            String color="";
            while(rs.next())
            	
            { i++;
            if (i%2==0)
            {
         	   color="gray";
            }
            else
            {
         	   color="white";
            }
              
           %>
					
					
					
				<tr style="background-color:<%=color%>;">
				
					<td><%=rs.getString("entrydate")%></td>
					
					
					<td>
					
					 <%
                 int catId = Integer.parseInt(""+rs.getBigDecimal("expensetype"));
                String sqlCat = "select expensetype from expense where id="+catId;
                Statement st2=con.createStatement();
                ResultSet rs2 = st2.executeQuery(sqlCat);
                rs2.next(); 
            	 exenseStr=rs2.getString("expensetype");
                 out.println(exenseStr); 
                
               %>


					</td>
						
					<td><%=rs.getString("paytoprty")%></td>
					<td><%=rs.getString("paymode")%></td>
					<td><%=rs.getString("amount")%></td>
					<td><%=rs.getString("ddno")%></td>
					<td><%=rs.getString("information")%></td>
					
					



					
		<td>			
	<form action="expenseMasterEditAction.jsp" method="post" >				
 
<input type="hidden" name="idhid" value="<%=rs.getInt("id")%>">
<input type="hidden" name="showdatehid" value="<%=rs.getDate("entrydate")%>">
<input type="hidden" name="expensenohid" value="<%=rs.getString("expenseno")%>">
<input type="hidden" name="expensehid" value="<%=rs2.getString("expensetype")%>">
<input type="hidden" name="paytoprtyhid" value="<%=rs.getString("paytoprty")%>">
<input type="hidden" name="paymodehid" value="<%=rs.getString("paymode")%>">
<input type="hidden" name="amounthid" value="<%=rs.getString("amount")%>">
<input type="hidden" name="ddnohid" value="<%=rs.getString("ddno")%>">
<input type="hidden" name="informationhid" value="<%=rs.getString("information")%>">
<input type="image" src="images/edit.png" height="15px"  width="15px" >

</form>

<%}
    
    
    st.close();
    con.close();
   }
   catch(Exception e)
    
   {
       out.println(e);
      
   }
    
  %>
</td>



				</tr>
        </tbody>
			</table>
		

				
        
 </body>
</html>