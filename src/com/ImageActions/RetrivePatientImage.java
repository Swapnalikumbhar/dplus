package com.ImageActions;


 

import java.io.*;
import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static com.dbcon.MyDriver.*;
/**
 *
 * @author TCT
 */
@WebServlet("/imageServlet")
public class RetrivePatientImage extends HttpServlet{

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		service(request, response);
	
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		service(request, response);
		
	}
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// gets values of text fields
		
		// deletefile("C:\\Users\\tct4\\workspace\\Doctorplus\\WebContent\\images\\test.jpg"); // Delete file deleteme.txt
		
				Connection con = null;
				String pid = request.getParameter("id");
				PrintWriter out = response.getWriter();
				Boolean flag = true;
				 BigDecimal id = null;
		         HttpSession session=request.getSession(false);  
		         if(session!=null){  
		        	 id = (BigDecimal) session.getAttribute("Clinic_Id");
		        	 System.out.println("id = " + id);
		         }
				try{
					 Class.forName(DB_DRIVER);
			            con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS);
			        //ArrayList<Double> arr = new ArrayList<Double>();
					Statement stmt = con.createStatement();
					ResultSet rs = stmt.executeQuery("select patient_profile_photo from clinic_patient where patient_id ="+pid+" and clinic_id="+id+";");
					//ResultSet rs1 = stmt.executeQuery("select patient_id from clinic_patient;");
					/*
					Statement st1=con.createStatement();
				     ResultSet rs1 = st1.executeQuery("select patient_id from clinic_patient");
				 
				       List li = new ArrayList();
				 
				       while(rs.next())
				       {
				           li.add(rs.getBigDecimal(1));
				           System.out.println("id = " + rs.getBigDecimal(1));
				       }
				 
				       Integer[] arr = new Integer[li.size()];
				       Iterator it = li.iterator();
				 
				       int i = 0;
				       while(it.hasNext())
				       {
				    	   //System.out.println("id = " + li.get(i));
				    	   i++;
				       }*/
					int i =0;
					while (rs.next()) {
						//System.out.println("in while loop....");

						InputStream in = rs.getBinaryStream(1);
						OutputStream f = new FileOutputStream(new File("F:\\abc\\Doctorplus\\WebContent\\images\\test.jpg"));
						i++;
						int c = 0;
						
						while ((c = in.read()) > -1) {
							//System.out.println("in while write loop....");
							f.write(c);
						}
						f.close();
						in.close();
						//System.out.println("out while write loop....");
						
						getServletContext().getRequestDispatcher("/imageServlet").forward(request, response);
						//getServletContext().getRequestDispatcher("/myimageServlet").include(request, response);
					}
				}catch(Exception ex){
					out.println(ex.getMessage());
					try {
						con.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				// forwards to the message page
				//IProject project = root.getProject(currentProjectName);
				//project.refreshLocal(IResource.DEPTH_INFINITE, null);
				//ResourcesPlugin.getWorkspace().getRoot().getProjects();
				//  response.sendRedirect("http://localhost:8080/Doctorplus/imageServlet1"); 
				getServletContext().getRequestDispatcher("/updatePatient.jsp").forward(request, response);
	
	}
	private void deletefile(String fileName) {
		 File file = new File(fileName);
		  boolean success = file.delete();
		  if (!success) {
		   System.out.println(fileName + " Deletion failed.");
		  } else {
		   System.out.println(fileName + " File deleted.");
		  }
		
		
	}

	
}
