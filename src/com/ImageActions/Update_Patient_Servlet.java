package com.ImageActions;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import static com.dbcon.MyDriver.*;

@WebServlet("/updateServlet")
@MultipartConfig(maxFileSize = 16177215)
public class Update_Patient_Servlet extends HttpServlet{
	
	
	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// gets values of text fields
		String pid =request.getParameter("id");
		System.out.println(pid);
		String pname=request.getParameter("ptname");
		System.out.println(pname);
    	String paddress=request.getParameter("ptaddress");
    	String pa = paddress.trim();
    	System.out.println(paddress);
    	String pdob=request.getParameter("dob");
    	System.out.println(pdob);
        String pemail=request.getParameter("email");
        System.out.println(pemail);
    	String pcontact=request.getParameter("contact");
    	System.out.println(pcontact);
    	String pgender=request.getParameter("gender");
    	System.out.println(pgender);
    	String pheight=request.getParameter("height");
    	System.out.println(pheight);
    	String pweight=request.getParameter("weight");
    	System.out.println(pweight);
    	String pbgroup=request.getParameter("bgroup");
    	System.out.println(pbgroup);
    	String pmstatus=request.getParameter("mstatus");
    	System.out.println(pmstatus);
    	String poccpation=request.getParameter("occupation");
    	System.out.println(poccpation);
    	 BigDecimal id = null;
         HttpSession session=request.getSession(false);  
         if(session!=null){  
        	 id = (BigDecimal) session.getAttribute("Clinic_Id");
        	 System.out.println("id = " + id);
         
         }
          
         /* StringBuffer paddress = new StringBuffer(request.getParameter("ptadd"));
          
          int loc = (new String(paddress)).indexOf('\n');
          while(loc > 0){
              paddress.replace(loc, loc+1, "<BR>");
              loc = (new String(paddress)).indexOf('\n');
         }
         System.out.println("address = " + paddress); */
		InputStream inputStream = null;	// input stream of the upload file
		
		// obtains the upload file part in this multipart request
		Part filePart = request.getPart("photo");
		if (filePart != null) {
			// prints out some information for debugging
			System.out.println(filePart.getName());
			System.out.println(filePart.getSize());
			System.out.println(filePart.getContentType());
			
			// obtains input stream of the upload file
			inputStream = filePart.getInputStream();
		}
		
		Connection conn = null;	// connection to the database
		String message = null;	// message will be sent back to client
		
		try {
			// connects to the database
			
			//String Sql="update clinic_patient set patient_name='"+pname+"',patient_address='"+pa+"',patient_contact="+pcontact+",patient_email='"+pemail+"',patient_dob=?,patient_gender='"+pgender+"',patient_marital_status='"+pmstatus+"',patient_height="+pheight+",patient_weight="+pweight+",patient_bloodgroup='"+pbgroup+"',patient_profile_photo =?,patient_occupation='"+poccpation+"' where patient_id="+pid+";";
			
			 String Sql="update clinic_patient set patient_name='"+pname+"',patient_address='"+pa+"',patient_contact="+pcontact+",patient_email='"+pemail+"',patient_dob=?,patient_gender='"+pgender+"',patient_marital_status='"+pmstatus+"',patient_height="+pheight+",patient_weight="+pweight+",patient_bloodgroup='"+pbgroup+"',patient_occupation='"+poccpation+"' where patient_id="+pid+" and clinic_id="+id;
				   
	          
	          
         System.out.println(Sql);
            // out.println("Sql query"+Sql +"<br/>");
            Class.forName(DB_DRIVER);
            Connection con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS);
            
            Statement st=con.createStatement();

            
          

              DateFormat df = new SimpleDateFormat("mm/dd/yyyy");
              
              java.util.Date pdate = df.parse(pdob);
            java.sql.Date d1= new java.sql.Date(pdate.getTime());
            
           	PreparedStatement statement = con.prepareStatement(Sql);
			/*statement.setString(1, pname);
			statement.setString(2, paddress);
			statement.setString(3, pcontact);
			statement.setString(4, pemail);
			statement.setDate(5, d1);
			statement.setString(6, pgender);
			statement.setString(7, pmstatus);
			statement.setString(8, pheight);
			statement.setString(9, pweight);
			statement.setString(10, pbgroup);
			*/
           	statement.setDate(1, d1);
           	statement.setBlob(2, inputStream);
			/*statement.setString(12, poccpation);
			statement.setString(13, pid);
			*/
			// sends the statement to the database server
			int row = statement.executeUpdate();
			if (row > 0) {
				message = "File uploaded and saved into database";
				System.out.println(message);
				
			}
		} catch (SQLException | ClassNotFoundException | ParseException ex) {
			message = "ERROR: " + ex.getMessage();
			ex.printStackTrace();
		} finally {
			if (conn != null) {
				// closes the database connection
				try {
					conn.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}
			// sets the message in request scope
			request.setAttribute("Message", message);
			
			// forwards to the message page
			getServletContext().getRequestDispatcher("/registerPatient.jsp").forward(request, response);
			
		}
	}
}