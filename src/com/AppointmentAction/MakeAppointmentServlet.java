package com.AppointmentAction;


import static com.dbcon.MyDriver.DB_DRIVER;
import static com.dbcon.MyDriver.DB_PASS;
import static com.dbcon.MyDriver.DB_URL;
import static com.dbcon.MyDriver.DB_USER;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.*;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/appointmentServlet")
@MultipartConfig(maxFileSize = 16177215)

public class MakeAppointmentServlet extends HttpServlet
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
	{
		String sname=request.getParameter("appname");
		String scontact=request.getParameter("appcontact");
		String sdate=request.getParameter("doapp");
		String sdetail=request.getParameter("appdetail");
		String stime1=request.getParameter("time");
		String stime2=request.getParameter("seconds");
		String sstatus=request.getParameter("status");
		
		
		  System.out.println("name = " + sname);
		  System.out.println("contact = " + scontact);
		  System.out.println("date = " + sdate);
		  System.out.println("detail = " + sdetail);
		  System.out.println("time = " + stime1);
		  System.out.println("time = " + stime2);
		
		 BigDecimal id = null;
         HttpSession session=request.getSession(false);  
         if(session!=null)
         {  
        	 id = (BigDecimal) session.getAttribute("Clinic_Id");
        	 System.out.println("id = " + id);
         }
         Connection conn = null;	// connection to the database
 		 String message = null;	// message will be sent back to client
 		 
 		 
 		 
 		try
 		{
 			String S1="insert into appointment(app_patient_name,app_contact,appointment_detail,appointment_date,appointment_time_hr,appointment_time_min,appointment_status,clinic_id) values(?,?,?,?,?,?,?,?)";
 			
 			 System.out.println(S1);
 			 
 			Class.forName(DB_DRIVER);
            Connection con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS);
            Statement st=con.createStatement();
        
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
               java.util.Date pdate = df.parse(sdate);
               System.out.println("date inserted= " + pdate);
             java.sql.Date d1= new java.sql.Date(pdate.getTime());
             System.out.println("date inserted= " + d1);
             
            	PreparedStatement statement = con.prepareStatement(S1);
            	
            	 System.out.println("in try");
            statement.setString(1, sname);
 			statement.setString(2,scontact);
 			
 			statement.setString(3, sdetail);
 			statement.setDate(4, d1);
 			statement.setString(5, stime1);
 			statement.setString(6, stime2);
 			statement.setString(7, sstatus);
 			
 			statement.setBigDecimal(8, id);
 			int row = statement.executeUpdate();
 			if (row > 0)
 			{
 				message = "File uploaded and saved into database";
 				System.out.println(message);
 				
 			}
 		}
 			
 			
 			
 			catch(SQLException | ClassNotFoundException | ParseException ex)
 	 		{
 				message = "ERROR: " + ex.getMessage();
 				ex.printStackTrace();
 			} 
 			finally 
 			{
 				if (conn != null)
 					{
 					// closes the database connection
 					try
 					{
 						conn.close();
 					} 
 					catch (SQLException ex)
 					{
 						ex.printStackTrace();
 					}
 				}
 				// sets the message in request scope
 				request.setAttribute("Message", message);
 				
 				// forwards to the message page
 				getServletContext().getRequestDispatcher("/manageAppointment.jsp").forward(request, response);
 				
	}
 		
	}
	
	

}
